import React, { useState} from 'react';
import { Button } from 'primereact/button';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';

export default function Projet() {
     
     const [projects, setProjects] = useState([]);

     const token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlJrTXdNelkyTlVWRU1VRXlPRGszT0RnM1JFSTFNVUUxTlRkRVF6QTFSRUV6TlVRNE5USTJNQSJ9.eyJodHRwOi8vbWlkYXMuZGVsbGEuYWkvdGVuYW50IjoidGVzdHRlY2huaXF1ZV9jbGFyaXNzZSIsImh0dHA6Ly9taWRhcy5kZWxsYS5haS91c2VyX25hbWUiOiJ0ZXN0dGVjaG5pcXVlX2NsYXJpc3NlQGNsaWVudC5kZWxsYS5haSIsImlzcyI6Imh0dHBzOi8vZGVsbGEtYWkuZXUuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDYxMzFjZGU5ODhmNDVhMDA3MmJkNzE3MSIsImF1ZCI6WyJodHRwOi8vbWlkYXMuZGVsbGEuYWkiLCJodHRwczovL2RlbGxhLWFpLmV1LmF1dGgwLmNvbS91c2VyaW5mbyJdLCJpYXQiOjE2MzA2NTcwODksImV4cCI6MTYzMTUyMTA4OSwiYXpwIjoiM25XaGVMQXZQaUJGNkNuRW1nMGdhZzlxanZReHlnUTQiLCJzY29wZSI6Im9wZW5pZCBwcm9maWxlIGVtYWlsIG9mZmxpbmVfYWNjZXNzIn0.obN7PCCBQhx8-EfMhWLZwMa-Rx6un5GvpBIeUCl2xkunwjvpogECIxHskC7ZRNAM8rXN9rnnXIjxWldt-9Hu-WvM0wxPcjCYyoKSFT5qEvZw4eJgHOcJ7mJJwHZ-pXl9Q2mfy9O-mx9IabnIftsF5I4Ns2oYA7FnOOoYktvw0lsvH1IDzXAprIlO1Nakeg7dhwDXsUCVdjn9rLnp41mcbS6IudI46eDkAyJDLmN9rGF0DNrJndIbDg5h96e7bltKbljivtZWuX-igi8oq7Q3now4LweTV6ew-hbEWqmMXNoDSgTQelyTyQBGstwBWl5sodZxHnbTM9e7pOuzmgeM2g';
          
     function searchProject(){  
          const options = {
               method:'GET',
               headers: {
                    'Accept' : 'application/json',
                'Content-Type' : 'application/json',
                'Authorization': `Bearer ${token}`
               },
               mode: 'cors'
          };        
          fetch('https://services-gateway.staging.azure.dellalegal.com/api/v3/projects', options)
          .then(response => response.json())
          .then(data => {
               console.log("Data");
               console.log(data.data.projects);
               setProjects(data.data.projects);
          })
          .catch(error => {
               console.error(error);
          });
     }

     function addPlaybook(){
          console.log('Ajout playbook');
     }

     const usersBodyTemplate = (rowData) => {
          return (
               <DataTable value={rowData.users}>
                         <Column field="userId" header="Id"></Column>
                         <Column field="role" header="Role"></Column>
                         <Column field="addedOn" header="Added on"></Column>
                    </DataTable>
          );
      }

      const actionsProjectTemplate = () => {
          return (
               <Button label="Ajouter un Playbook" onClick={addPlaybook} />
          );
      }
     return (
          <div className="ui-g">
               <div className="ui-g-12">
                    <Button label="Charger la liste des projets" onClick={searchProject} />
               </div>
               <br/>
               <div className="ui-g-12">
                    {projects.length > 0 ?
                    (<DataTable value={projects}>
                         <Column field="id" header="Id"></Column>
                         <Column field="name" header="Name"></Column>
                         <Column field="status" header="Status"></Column>
                         <Column header="Users" body={usersBodyTemplate}></Column>
                         <Column header="" body={actionsProjectTemplate}></Column>         
                    </DataTable>) : (
                         <p>{projects.length} projets</p>
                    )
               }
                    
               </div>
          </div>
     )
}
