import React from 'react';
import { TabView, TabPanel} from 'primereact/tabview';
import Projet from './Projet';
import Admin from './Admin';

export default function Accueil() {

     return (
          
          <div className="ui-g">
               <TabView>
               <TabPanel header="Projets">
                    <Projet />
               </TabPanel>
               <TabPanel header="Création">
                    <Admin />
               </TabPanel>
          </TabView>
          </div>
     )
}
