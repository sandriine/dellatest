import React, {useState} from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';

export default function Admin() {
     const [nameProject, setNameProject] = useState('');

     const token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlJrTXdNelkyTlVWRU1VRXlPRGszT0RnM1JFSTFNVUUxTlRkRVF6QTFSRUV6TlVRNE5USTJNQSJ9.eyJodHRwOi8vbWlkYXMuZGVsbGEuYWkvdGVuYW50IjoidGVzdHRlY2huaXF1ZV9jbGFyaXNzZSIsImh0dHA6Ly9taWRhcy5kZWxsYS5haS91c2VyX25hbWUiOiJ0ZXN0dGVjaG5pcXVlX2NsYXJpc3NlQGNsaWVudC5kZWxsYS5haSIsImlzcyI6Imh0dHBzOi8vZGVsbGEtYWkuZXUuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDYxMzFjZGU5ODhmNDVhMDA3MmJkNzE3MSIsImF1ZCI6WyJodHRwOi8vbWlkYXMuZGVsbGEuYWkiLCJodHRwczovL2RlbGxhLWFpLmV1LmF1dGgwLmNvbS91c2VyaW5mbyJdLCJpYXQiOjE2MzA2NTcwODksImV4cCI6MTYzMTUyMTA4OSwiYXpwIjoiM25XaGVMQXZQaUJGNkNuRW1nMGdhZzlxanZReHlnUTQiLCJzY29wZSI6Im9wZW5pZCBwcm9maWxlIGVtYWlsIG9mZmxpbmVfYWNjZXNzIn0.obN7PCCBQhx8-EfMhWLZwMa-Rx6un5GvpBIeUCl2xkunwjvpogECIxHskC7ZRNAM8rXN9rnnXIjxWldt-9Hu-WvM0wxPcjCYyoKSFT5qEvZw4eJgHOcJ7mJJwHZ-pXl9Q2mfy9O-mx9IabnIftsF5I4Ns2oYA7FnOOoYktvw0lsvH1IDzXAprIlO1Nakeg7dhwDXsUCVdjn9rLnp41mcbS6IudI46eDkAyJDLmN9rGF0DNrJndIbDg5h96e7bltKbljivtZWuX-igi8oq7Q3now4LweTV6ew-hbEWqmMXNoDSgTQelyTyQBGstwBWl5sodZxHnbTM9e7pOuzmgeM2g';


     function createProjet(){
          console.log(nameProject);
          const options = {
               method:'POST',
               headers: {
                    'Accept' : 'application/json',
                'Content-Type' : 'application/json',
                'Authorization': `Bearer ${token}`
               },
               mode: 'cors',
               body: JSON.stringify({'name': nameProject})
          };
          fetch('https://services-gateway.staging.azure.dellalegal.com/api/v3/projects', options)
          .then(response => response.json())
          .then(data => {
               console.log("Data");
               console.log(data);
          })
          .catch(error => {
               console.error(error);
          });

     }

     return (
          <div>
               <div className="ui-g-12">
                    <InputText value={nameProject} onChange={(e) => setNameProject(e.target.value)} />
                    <Button label="Créer un projet" onClick={createProjet} />
               </div>
          </div>
     )
}
